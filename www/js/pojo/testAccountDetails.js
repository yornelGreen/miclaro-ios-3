var testAccountDetails = {
    qualification: {
        RedeemProgram: false,
        Netflix: false,
        RefererResponse: {
            registerUpdated: false,
            paperless: true,
            solvent: false
        },
        NetflixResponse: {
            solvent: false,
            Account_Status: true,
            CreditLimit: true
        }
    },
    AccounInfo: {
        addressLinesField: {
            line1Field: '',
            line2Field: '6315 CALLE PACIFICO',
            PropertyChanged: null
        },
        invoicesField: [],
        bANField: 683678651,
        sSNField: '************',
        emailField: 'axel.torres@claropr.com',
        contractMonthsField: '1',
        accountTypeField: 'I2',
        accountSubtypeField: '4',
        banStatusField: 'O',
        billCycleField: 8,
        billDateField: '02/04/2019',
        billDueDateField: '03/01/2019',
        billBalanceField: '1,505.15',
        pastDueAmountField: 1505.15,
        lastPaymentDateField: null,
        lastPaymentAmountField: 0,
        cycleStartDateField: '02/04/2019',
        cycleEndDateField: '03/03/2019',
        cycleDateField: 'Feb 4 a Mar 3',
        cycleDaysLeftField: '5',
        creditClassField: 'C',
        creditBalanceField: '  350.00',
        firstNameField: 'LUIS',
        lastNameField: 'MEDINA',
        addressField: ' 6315 CALLE PACIFICO',
        cityField: 'PONCE',
        stateField: 'PR',
        countryField: 'USA',
        zIPField: '00728',
        invoiceURLField: null,
        paperlessField: true,
        hasErrorField: false,
        errorDescField: null,
        errorNumField: 0,
        PropertyChanged: null
    },
    SubscriberInfo: [
        {
            equipmentInfoField: {
                itemIdField: '54392H',
                itemNameField: null,
                itemImageField: 'https://miclaro.clarotodo.com/ImageHandler/OracleImageHandler.ashx?DEVICE_ID=54392H',
                itemDescriptionField: 'S7 EDGE 32GB',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'HEXL170L',
                sOCDescriptionField: 'Plan Nacional : Voz - Mensajes - LD ilimitado a USA - Roaming - DPRUSILIM',
                sOCDescriptionInfoField: '<ul><li>Voz</li><li>Mensajes</li><li>LD ilimitado a USA</li><li>Roaming</li><li>DPRUSILIM</li></ul>',
                relatedSocCodeField: 'not_set',
                socRateField: 0,
                totalRateField: 0,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '06/04/2018',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '17879100794',
                            offerIdField: 'UNLALL_TETH',
                            balanceIdField: 'PR_TETH_BalanceDef_10176_NP',
                            balanceAmountField: '0',
                            balanceAmountUnitField: '0 MB',
                            effectiveDateField: '2019-02-03T00:00:00',
                            expiryDateField: '2019-03-03T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100794',
                        quotaField: '16106127360',
                        quotaTextField: '15 GB',
                        displayNameField: 'HOTSPOT',
                        nameField: 'UNLALL_TETH',
                        offerIdField: 'UNLALL_TETH',
                        usedField: '0',
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100794',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: 'IM Ilimitado No Reduccion PRUS',
                        nameField: 'UNLALL',
                        offerIdField: 'UNLALL',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100794',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'POS LIMITED ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '52896H',
                        equipmentTypeField: 'S',
                        eSNField: '89011101171214190371',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '54392H',
                        equipmentTypeField: 'J',
                        eSNField: '353556085114324',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50461H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100132207953283',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '40695H',
                        equipmentTypeField: 'J',
                        eSNField: '354595061587268',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '6/2/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MOVILL',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'ROAMUSA',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDINT',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDUSAG',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '2/10/2015 11:00:00 PM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '7879100794',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: '54392H',
                itemNameField: null,
                itemImageField: 'https://miclaro.clarotodo.com/ImageHandler/OracleImageHandler.ashx?DEVICE_ID=54392H',
                itemDescriptionField: 'S7 EDGE 32GB',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'HEXL170L',
                sOCDescriptionField: 'Plan Nacional : Voz - Mensajes - LD ilimitado a USA - Roaming - DPRUSILIM',
                sOCDescriptionInfoField: '<ul><li>Voz</li><li>Mensajes</li><li>LD ilimitado a USA</li><li>Roaming</li><li>DPRUSILIM</li></ul>',
                relatedSocCodeField: 'not_set',
                socRateField: 0,
                totalRateField: 0,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '06/04/2018',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100892',
                        quotaField: '16106127360',
                        quotaTextField: '15 GB',
                        displayNameField: 'HOTSPOT',
                        nameField: 'UNLALL_TETH',
                        offerIdField: 'UNLALL_TETH',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: '17879100892',
                            offerIdField: 'UNLALL',
                            balanceIdField: 'NA_BalanceDef_CUNL_10176',
                            balanceAmountField: '17621739893',
                            balanceAmountUnitField: '16.41 GB',
                            effectiveDateField: '2019-02-03T00:00:00',
                            expiryDateField: '2019-03-03T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100892',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: 'IM Ilimitado No Reduccion PRUS',
                        nameField: 'UNLALL',
                        offerIdField: 'UNLALL',
                        usedField: '17621739893',
                        usedTextField: '16.41 GB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '7879100892',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'POS LIMITED ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '52896H',
                        equipmentTypeField: 'S',
                        eSNField: '89011101171214189308',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '54392H',
                        equipmentTypeField: 'J',
                        eSNField: '353773081866910',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50451H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100132205272462',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '51758G',
                        equipmentTypeField: 'J',
                        eSNField: '359234060605815',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '6/2/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MOVILL',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'ROAMUSA',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDINT',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDUSAG',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'SEGU1199',
                        relatedSocCodeField: '',
                        effectiveDateField: '2/24/2015 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '2/10/2015 11:00:00 PM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '7879100892',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: '53036H',
                itemNameField: null,
                itemImageField: 'https://miclaro.clarotodo.com/ImageHandler/OracleImageHandler.ashx?DEVICE_ID=53036H',
                itemDescriptionField: 'S7 EDGE 32GB',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'HEXL170L',
                sOCDescriptionField: 'Plan Nacional : Voz - Mensajes - LD ilimitado a USA - Roaming - DPRUSILIM',
                sOCDescriptionInfoField: '<ul><li>Voz</li><li>Mensajes</li><li>LD ilimitado a USA</li><li>Roaming</li><li>DPRUSILIM</li></ul>',
                relatedSocCodeField: 'not_set',
                socRateField: 0,
                totalRateField: 0,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '06/04/2018',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '19392429485',
                            offerIdField: 'UNLALL_TETH',
                            balanceIdField: 'PR_TETH_BalanceDef_10176_NP',
                            balanceAmountField: '0',
                            balanceAmountUnitField: '0 MB',
                            effectiveDateField: '2019-02-03T00:00:00',
                            expiryDateField: '2019-03-03T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429485',
                        quotaField: '16106127360',
                        quotaTextField: '15 GB',
                        displayNameField: 'HOTSPOT',
                        nameField: 'UNLALL_TETH',
                        offerIdField: 'UNLALL_TETH',
                        usedField: '0',
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE POS NT_NO UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429485',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: 'IM Ilimitado No Reduccion PRUS',
                        nameField: 'UNLALL',
                        offerIdField: 'UNLALL',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE POS NT_NO UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429485',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'LIMITED POS ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '52896H',
                        equipmentTypeField: 'S',
                        eSNField: '89011101171214283051',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '53036H',
                        equipmentTypeField: 'J',
                        eSNField: '354315084153604',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50461H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100132207960528',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50170',
                        equipmentTypeField: 'I',
                        eSNField: '358569037662048',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '40910H',
                        equipmentTypeField: 'J',
                        eSNField: '358316073534032',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '6/2/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MOVILL',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'ROAMUSA',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDINT',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDUSAG',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'SEGU1199',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/13/2014 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/24/2010 12:00:00 AM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '9392429485',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: '30060H',
                itemNameField: null,
                itemImageField: 'https://miclaro.clarotodo.com/ImageHandler/OracleImageHandler.ashx?DEVICE_ID=30060H',
                itemDescriptionField: 'IPH 7 32GB',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'HEXL170L',
                sOCDescriptionField: 'Plan Nacional : Voz - Mensajes - LD ilimitado a USA - Roaming - DPRUSILIM',
                sOCDescriptionInfoField: '<ul><li>Voz</li><li>Mensajes</li><li>LD ilimitado a USA</li><li>Roaming</li><li>DPRUSILIM</li></ul>',
                relatedSocCodeField: 'not_set',
                socRateField: 0,
                totalRateField: 0,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '06/04/2018',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '19392429486',
                            offerIdField: 'UNLALL_TETH',
                            balanceIdField: 'PR_TETH_BalanceDef_10176_NP',
                            balanceAmountField: '0',
                            balanceAmountUnitField: '0 MB',
                            effectiveDateField: '2019-02-03T00:00:00',
                            expiryDateField: '2019-03-03T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429486',
                        quotaField: '16106127360',
                        quotaTextField: '15 GB',
                        displayNameField: 'HOTSPOT',
                        nameField: 'UNLALL_TETH',
                        offerIdField: 'UNLALL_TETH',
                        usedField: '0',
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429486',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: 'IM Ilimitado No Reduccion PRUS',
                        nameField: 'UNLALL',
                        offerIdField: 'UNLALL',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE NT_NO POS UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429486',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'POS LIMITED ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '52896H',
                        equipmentTypeField: 'S',
                        eSNField: '89011101171214189324',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '30060H',
                        equipmentTypeField: 'J',
                        eSNField: '353803086644606',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50461H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100132207953234',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50170',
                        equipmentTypeField: 'I',
                        eSNField: '358569037657212',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50261',
                        equipmentTypeField: 'I',
                        eSNField: '357408040102703',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50714H',
                        equipmentTypeField: 'J',
                        eSNField: '356004060487842',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '6/2/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MOVILL',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'ROAMUSA',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDINT',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDUSAG',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'SEGU1199',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/13/2014 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/24/2010 12:00:00 AM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '9392429486',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        },
        {
            equipmentInfoField: {
                itemIdField: '53010H',
                itemNameField: null,
                itemImageField: 'https://miclaro.clarotodo.com/ImageHandler/OracleImageHandler.ashx?DEVICE_ID=53010H',
                itemDescriptionField: 'J1 2016',
                pricecodeField: null,
                installmentsField: null,
                installmentValueField: null,
                PropertyChanged: null
            },
            planInfoField: {
                sOCInfoField: 'HEXL170L',
                sOCDescriptionField: 'Plan Nacional : Voz - Mensajes - LD ilimitado a USA - Roaming - DPRUSILIM',
                sOCDescriptionInfoField: '<ul><li>Voz</li><li>Mensajes</li><li>LD ilimitado a USA</li><li>Roaming</li><li>DPRUSILIM</li></ul>',
                relatedSocCodeField: 'not_set',
                socRateField: 0,
                totalRateField: 0,
                commitmentStartDateField: '0',
                mCommitmentOrigNoMonthField: '0',
                commitmentEndDateField: '0',
                effectiveDateField: '06/04/2018',
                PropertyChanged: null
            },
            usageInfoField: {
                dataOffersField: [
                    {
                        balancesField: {
                            subscriberIdField: '19392429487',
                            offerIdField: 'UNLALL_TETH',
                            balanceIdField: 'PR_TETH_BalanceDef_10176_NP',
                            balanceAmountField: '0',
                            balanceAmountUnitField: '0 MB',
                            effectiveDateField: '2019-02-03T00:00:00',
                            expiryDateField: '2019-03-03T00:00:00',
                            quotaRolloverField: '0',
                            quotaRolloverTextField: '0 OM',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429487',
                        quotaField: '16106127360',
                        quotaTextField: '15 GB',
                        displayNameField: 'HOTSPOT',
                        nameField: 'UNLALL_TETH',
                        offerIdField: 'UNLALL_TETH',
                        usedField: '0',
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE POS NT_NO UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429487',
                        quotaField: '644245094400',
                        quotaTextField: '600 GB',
                        displayNameField: 'IM Ilimitado No Reduccion PRUS',
                        nameField: 'UNLALL',
                        offerIdField: 'UNLALL',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'BASE POS NT_NO UNLIMITED COMBOFFER_10165',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    },
                    {
                        balancesField: {
                            subscriberIdField: null,
                            offerIdField: null,
                            balanceIdField: null,
                            balanceAmountField: null,
                            balanceAmountUnitField: null,
                            effectiveDateField: null,
                            expiryDateField: null,
                            quotaRolloverField: null,
                            quotaRolloverTextField: '',
                            PropertyChanged: null
                        },
                        subscriberIdField: '9392429487',
                        quotaField: '20971525',
                        quotaTextField: '20 MB',
                        displayNameField: 'Roaming Data a Granel',
                        nameField: 'ROAMPYG',
                        offerIdField: 'ROAMPYG',
                        usedField: null,
                        usedTextField: '0 MB',
                        offerGroupField: 'LIMITED POS ROAMINGADDITIONAL NT_ROAM_GRANEL',
                        groupIDField: null,
                        endDateField: null,
                        PropertyChanged: null
                    }
                ],
                localBalanceField: '0',
                localTopField: '0MB',
                localDescriptionField: 'not_set',
                localPUJField: 'N',
                pUJTOPField: '0MB',
                roamBalanceField: '0',
                roamTopField: '0',
                minutesUsageField: '0',
                sMSUSageField: '0',
                mMSUsageField: '0',
                sMSPremiunUsageField: '0',
                roamingUsageField: '0',
                lDUsageField: '0',
                lDIUsageField: '0',
                notificationField: '0%',
                PropertyChanged: null
            },
            additionalpackagesField: {
                localPackagesField: [],
                roamingPackagesField: [],
                PropertyChanged: null
            },
            servEquipmentSerialsField: {
                servEquipmentSerialField: [
                    {
                        itemIdField: '52896H',
                        equipmentTypeField: 'S',
                        eSNField: '89011101171214189316',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '53010H',
                        equipmentTypeField: 'J',
                        eSNField: '357014075234823',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '40283H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100121139423572',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50461H',
                        equipmentTypeField: 'S',
                        eSNField: '89011100132207960593',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50170',
                        equipmentTypeField: 'I',
                        eSNField: '358569037663301',
                        PropertyChanged: null
                    },
                    {
                        itemIdField: '50714H',
                        equipmentTypeField: 'J',
                        eSNField: '356004060503044',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            servSVAInfoField: {
                servSVAsField: [
                    {
                        sOCInfoField: 'G911',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'UNLENHANC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MOVILL',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'GUNSMSOC',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'ROAMUSA',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDINT',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'LDUSAG',
                        relatedSocCodeField: 'HEXL170L',
                        effectiveDateField: '6/4/2018 12:00:00 AM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'SMSDONTG',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/10/2017 11:00:00 PM',
                        PropertyChanged: null
                    },
                    {
                        sOCInfoField: 'MAIL2WD',
                        relatedSocCodeField: '',
                        effectiveDateField: '3/24/2010 12:00:00 AM',
                        PropertyChanged: null
                    }
                ],
                PropertyChanged: null
            },
            subscriberNumberField: '9392429487',
            productTypeField: 'G',
            subscriberStatusField: 'A',
            prepaidBalanceField: null,
            localBalanceField: null,
            reasonCodeField: 'FPNC',
            groupIDField: null,
            hasErrorField: false,
            errorDescField: null,
            errorNumField: 0,
            PropertyChanged: null
        }
    ],
    HasError: false,
    ErrorDesc: null,
    ErrorNum: 0
}